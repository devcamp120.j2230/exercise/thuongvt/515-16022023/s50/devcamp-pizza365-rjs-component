import { Component } from "react";
import ModalConfim from "./ModalConfim/ModalConfim";
import ModalInformation from "./ModalInformation/ModalInformation";

class ModalComponent extends Component {
    render(){
        return(
            <>
            <ModalInformation/>
            <ModalConfim/>
            </>
        )
    }
}

export default ModalComponent
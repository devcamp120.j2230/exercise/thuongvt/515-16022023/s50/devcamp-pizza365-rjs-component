import { Component } from "react";

class ModalInformation extends Component {
    render() {
        return (
            <>
                {/* <!--Vùng Modal--> */}
                <div id="user-modal" className="modal fade" tabindex="-1">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Thông tin đơn hàng</h5>
                            </div>
                            {/* <!-- Modal body - chứa form dữ liệu --> */}
                            <div className="modal-body">
                                <form>
                                    {/* <!-- form thiết kế như form bình thường, theo grid model --> */}
                                    <div className="row border-secondary">
                                        <div className="col-sm-12 form-group  border-info">
                                            <label for="">Họ và Tên</label>
                                            <input type="text" className="form-control" id="id-ho-ten" placeholder="...." />
                                        </div>
                                        <div className="col-sm-12 form-group border-info">
                                            <label for="">Số điện Thoại</label>
                                            <input type="text" id="id-dien-thoai" className="form-control" placeholder="...." />
                                        </div>
                                        <div className="col-sm-12 form-group  border-info">
                                            <label for="">Địa chỉ</label>
                                            <input type="text" className="form-control" id="id-dia-chi" placeholder="...." />
                                        </div>
                                        <div className="col-sm-12 form-group border-info">
                                            <label for="">Lời nhắn</label>
                                            <input type="text" className="form-control" id="inp-message" placeholder="...." />
                                        </div>
                                        <div className="col-sm-12 form-group  border-info">
                                            <label for="">mã giảm giá </label>
                                            <input type="text" id="id-ma-giam-gia" className="form-control" placeholder="...." />
                                        </div>
                                        <div className="col-sm-12 form-group  border-info">
                                            <label for="">Thông tin chi tiết</label>
                                            <textarea name="" id="id-chi-tiet" cols="10" rows="5" type="text" className="form-control"
                                                placeholder="...."></textarea>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" id="btn-modal-add" className="btn btn-primary">Tạo đơn</button>
                                <button type="button" id="btn-modal-cancel" className="btn btn-info" data-dismiss="modal">Quay lại</button>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default ModalInformation
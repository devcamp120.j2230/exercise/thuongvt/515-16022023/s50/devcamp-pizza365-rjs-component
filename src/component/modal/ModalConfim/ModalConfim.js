import { Component } from "react";

class ModalConfim extends Component {
    render() {
        return (
            <>
                {/* <!-- Modal --> */}
                <div id="user-modal-orderId" className="modal fade" tabindex="-1">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                            </div>
                            {/* <!-- Modal body - chứa form dữ liệu --> */}
                            <div className="modal-body">
                                <form>
                                    {/* <!-- form thiết kế như form bình thường, theo grid model --> */}
                                    <div className="col-sm-12 form-group bg-warning p-4">
                                        <p>Cảm ơn bạn đã đặt hàng tai Pizza 365. mã đơn hàng của bạn là:</p>
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Mã đơn hàng </label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" id="id-goderid" className="form-control" placeholder="...." />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default ModalConfim
import { Component } from "react";
import Image1 from "../../../assets/images/1.jpg"
import ImageBacon from "../../../assets/images/bacon.jpg"
import ImageHawai from "../../../assets/images/hawaiian.jpg"

class TypeComponent extends Component {
    render() {
        return (
            <>
                {/* <!-- thanh tiêu dề --> */}
                <div className="row">
                    <div className="col-sm-12 text-center p-4 mt-4 " style={{ color: "#ff5722" }}>
                        <h2><b className="p-1 border-bottom">Chọn loại pizza</b></h2>
                    </div>
                </div>
                {/* <!-- thanh nội dung --> */}
                <div className="col-sm-12">
                    <div className="row">
                        <div className="col-sm-4">
                            <div className="card" style={{ width: "18rem" }}>
                                <img className="card-img-top" src={Image1} alt="Card cap" />
                                <div className="card-body">
                                    <h6 className="card-title">OCEAN MANINA</h6>
                                    <p><samp>PIZZA HẢI SẢN SỐT VỚI MAYONNAISEA</samp></p>
                                    <p className="card-text">Pizza mang hương vị của Hải sản.</p>
                                </div>
                                <div className="card-footer text-center">
                                    <button className="btn form-control " id="btn-haisan" style={{ backgroundColor: "#ff5722" }}>chọn</button>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-4">
                            <div className="card" style={{ width: "18rem" }}>
                                <img className="card-img-top" src={ImageBacon} alt="Card cap" />
                                <div className="card-body">
                                    <h6 className="card-title">HAIWAIIN</h6>
                                    <p><samp>PIZZA DĂM BÔNG DỨA KIỂU HAWAI</samp></p>
                                    <p className="card-text">Pizaa mang hương vị biển Hawai</p>
                                </div>
                                <div className="card-footer text-center">
                                    <button className="btn form-control " id="btn-hawai" style={{ backgroundColor: "#ff5722" }}>chọn</button>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-4">
                            <div className="card" style={{ width: "18rem" }}>
                                <img className="card-img-top" src={ImageHawai} alt="Card cap" />
                                <div className="card-body">
                                    <h6 className="card-title">CHEESY CHICKEN BACON </h6>
                                    <p><samp>PIZZA GÀ PHO MAI THỊT HEO XÔNG KHÓI</samp></p>
                                    <p className="card-text">Loại Pizza truyền thống Italian.</p>
                                </div>
                                <div className="card-footer text-center">
                                    <button className="btn form-control" id="btn-chicken" style={{ backgroundColor: "#ff5722" }}>chọn</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default TypeComponent
import { Component } from "react";
import Image1 from "../../../assets/images/1.jpg"
import ImageBacon from "../../../assets/images/bacon.jpg"
import ImageHawai from "../../../assets/images/hawaiian.jpg"

class CarouselComponent extends Component {
    render() {
        return (
            <div>
                {/* Truly */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="row">
                            <div className="col-sm-12">
                                <h3><b>Pizza 365</b></h3>
                                <h5 style={{ fontStyle: "italic" }}>Truly italian !</h5>
                            </div>
                        </div>
                    </div>
                </div>
                    {/* carousel */}
                <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
                    <ol className="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img className="d-block w-100" src="" alt="First slide" />
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src={Image1} alt="Second slide" />
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src={ImageBacon} alt="Third slide" />
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src={ImageHawai} alt="Third slide" />
                        </div>
                    </div>
                    <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>
                <br></br>
            </div>
        )
    }
}

export default CarouselComponent
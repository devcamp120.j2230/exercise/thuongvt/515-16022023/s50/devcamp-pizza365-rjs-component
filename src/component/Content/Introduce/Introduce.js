import { Component } from "react";

class IntroduceComponent extends Component {
    render() {
        return (
            <div>
                {/* <!-- thanh tiêu dề --> */}
                <div className="col-sm-12 text-center p-4 mt-4">
                    <h3><b className="p-2 border-bottom" style={{ color: "#ff5722" }}>Tại sao lại Pizza 365:</b></h3>
                </div>
                {/* <!-- thanh nội dung --> */}
                <div className="col-sm-12">
                    <div className="row">
                        <div className="col-sm-3 p-4" style={{ backgroundColor: "lightgoldenrodyellow" }}>
                            <h3 className="p-2">Giới thiệu</h3>
                            <p className="p-2">Là quán phục vụ Pizza chuẩn thương hiệu Italian </p>
                        </div>
                        <div className="col-sm-3 p-4 " style={{ backgroundColor: "yellow" }}>
                            <h3 className="p-2">Phụ vụ</h3>
                            <p className="p-2">Phục vụ các món Pizza với các loại hương vị khác nhau mang tới trải nghiệm thú vị</p>
                        </div>
                        <div className="col-sm-3 p-4 " style={{ backgroundColor: "lightsalmon" }}>
                            <h3 className="p-2">Độc quyền</h3>
                            <p className="p-2">Là nhà hàng duy nhất tại Hà Nội được nhượng quyền từ Italian.</p>
                        </div>
                        <div className="col-sm-3 p-4" style={{ backgroundColor: "orange" }}>
                            <h3 className="p-2">Chất lượng</h3>
                            <p className="p-2">Mọi nguyên liêu được nhập khẩu chính hãng mang tới trải nghiệm người dùng với hương vị tối ưu
                                nhất.</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default IntroduceComponent
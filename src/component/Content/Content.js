import { Component } from "react";
import CarouselComponent from "./Carousel/Carousel";
import FromComponent from "./Form/From";
import IntroduceComponent from "./Introduce/Introduce";
import SizeComponent from "./Size/Size";
import TypeComponent from "./Type/Type";


class Content extends Component {
    render(){
        return(
            <>
            <CarouselComponent/>
            <IntroduceComponent/>
            <SizeComponent/>
            <TypeComponent/>
            <FromComponent/>
            </>  
        )
    }
}

export default Content